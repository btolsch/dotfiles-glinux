ZSH_TMUX_COMMAND="tmx2"
source $HOME/dotfiles/base/.zshrc

[[ -e ~/.fzf.zsh ]] && source ~/.fzf.zsh
[[ -e /etc/bash_completion.d/g4d ]] && source /etc/bash_completion.d/g4d
