#!/bin/zsh

single_key_prompt() {
  old_stty=$(stty -g)
  stty raw -echo; answer=$(head -c 1); stty $old_stty
  echo $answer
}

SELF_DIR=$(dirname $(realpath $0))
pushd $SELF_DIR

if [ -e /etc/fonts/conf.d/70-no-bitmaps.conf ]; then
  echo -n "remove 70-no-bitmaps.conf to use bitmap fonts? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    sudo rm -f /etc/fonts/conf.d/70-no-bitmaps.conf
  fi
fi

if ! dpkg -l clang-format &>/dev/null; then
  echo -n "install clang-format? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    sudo apt install clang-format
    echo "if /usr/lib/clang-format/clang-format.py doesn't exist now, you're gonna have a bad time"
  fi
fi

if [ ! -e ~/.local/share/fonts/siji ]; then
  echo -n "install siji font? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    mkdir -p ~/.local/share/fonts
    pushd ~/.local/share/fonts
    git clone https://github.com/stark/siji.git
    popd
    fc-cache -vf ~/.local/share/fonts &>/dev/null
  fi
fi

typeset -A glinux_symlink_files
typeset -A glinux_symlink_prefixes
glinux_symlink_files=(
  ".config/fontconfig/fonts.conf" "fonts.conf"
  ".config/nvim/init.vim" ".vimrc"
  ".xsessionrc" ".xinitrc"
)
glinux_symlink_prefixes=(
  ".xsessionrc" ".."
)
PREFIX=$(realpath --relative-to=$HOME/dotfiles $SELF_DIR)
for path_ in ${(k)glinux_symlink_files}; do
  if [[ "${platform_symlink_files[$path_]}" = "" ]]; then
    platform_symlink_files[$path_]=$glinux_symlink_files[$path_]
    platform_symlink_prefixes[$path_]=$PREFIX/$glinux_symlink_prefixes[$path_]
  fi
done
for file in $(cat <(ls -d .* | grep -v ".git") <(ls bin/*)); do
  if [ "${platform_symlink_files[$file]}" = "" ]; then
    platform_symlink_files[$file]=$file
    platform_symlink_prefixes[$file]=$PREFIX
  fi
done

popd
