source $HOME/dotfiles/base/zshenv_functions.zsh

export PATH=$(add_paths_after ${PATH:-:} $HOME/code/depot_tools chromium/src/libassistant/internal/build $HOME/code/eureka/vendor/eureka/tools/bin)
export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox
export LD_LIBRARY_PATH=$(add_paths_before ${LD_LIBRARY_PATH:-:} $HOME/.local/lib)
export USE_GOMA=1

source $HOME/dotfiles/base/.zshenv
